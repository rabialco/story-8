from django.test import TestCase, Client
from django.urls import resolve
from .views import home

# Create your tests here.
class Story7_UnitTest(TestCase):
    def test_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)
    
    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home)