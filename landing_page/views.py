from django.shortcuts import render
from django.http import JsonResponse
import json

# Create your views here.
def home(request):
    return render(request, 'index.html')

# def data(request):
# 	try:
# 		q = request.GET['q']
# 	except:
# 		q = 'quilting'
# 	jsonInput = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + q).json()

# 	return JsonResponse(jsonInput)